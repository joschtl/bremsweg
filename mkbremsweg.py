#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse, os, sys, math

# Konstanten und Defaultwerte
obsWidth = 113
obsHeight = 150
sReaCol = "55ff88"
sBrCol = "ffaa00"
sAnhCol = "5555ff"
roadCol = "555555"
vRestCol = "000000"
tRea = 1
rand = 50
titel="Anhalteweg"
untertitel="bei verschiedenen Geschwindigkeiten"
stdFont ="FreeSans"
signFont="din1451m.ttf"

infotext = """Quellen:
¹) Standard-Verzögerung laut VCD: -6,9444… m/s² (entspricht 25 km/h pro Sekunde)
²) Berechnungen, Formeln und Anregungen von Johannes Strommers Webseite:
   https://www.johannes-strommer.com/rechner/bremsweg-beschleunigung-geschwindigkeit/
³) Bilder geklaut von den Verkehrszeichen auf Wikipedia:
   https://de.wikipedia.org/wiki/Bildtafel_der_Verkehrszeichen_in_der_Bundesrepublik_Deutschland_seit_2017
⁴) Diese Option wählt das Fahrzeugbild aus, das auf die Abmessungen des jeweiligen Fahrzeugs skaliert wird.
   Die Abmessungen können mit --l1 und --h1 angepasst werden.
   Es ist am besten, das generische Auto vom Verkehrsschild zu verwenden und nur in begründeten Ausnahmen
   die realen Silhouetten existierender Autos, denn dann sollten auch angepasste Verzögerungen zum Einsatz
   kommen.
⁵) Bild von hollma@x0r.be

Anregungen an @joschtl@chaos.social oder per Mail an bremswrdlweg.20.pcb@a-bc.net.
"""

parser = argparse.ArgumentParser(description='Generiert ein Bild von zwei bremsenden Fahrzeugen mit verschiedener Geschwindigkeit vor einem Hindernis. Ausgewiesen werden Brems-, Reaktions- und Anhalteweg sowie Restgeschwindigkeit des schnelleren Fahrzeugs. Durch Anpassen der Geschwindigkeiten und Beschleunigungen können z.B. Anhaltewege bei verschiedenen Geschwindigkeiten, Straßenbeschaffenheiten, Aufmerksamkeitsleveln und ähnlichem verglichen werden.')
parser.add_argument("--width", help="Bildbreite, Defaultwert: 1024. Am besten so lassen, wenn es nicht unmöglich aussieht", dest='width', action="store", type=int, default = 1024)
parser.add_argument("--height", help="Bildhöhe, Defaultwert: 600. Am besten so lassen, wenn es nicht unmöglich aussieht", dest='height', action="store", type=int, default = 600)
parser.add_argument("--r1", help="Reaktionszeit des 1. Fahrzeugführenden in Sekunden, Defaultwert: 1", dest='r1', action="store", type=float, default = 1)
parser.add_argument("--r2", help="Reaktionszeit des 2. Fahrzeugführenden in Sekunden, Defaultwert: 1", dest='r2', action="store", type=float, default = 1)
parser.add_argument("--v1", help="Anfangsgeschwindigkeit Fahrzeug 1 in km/h, Defaultwert: 30", dest='v1', action="store", type=int, default = 30)
parser.add_argument("--v2", help="Anfangsgeschwindigkeit Fahrzeug 2 in km/h, Defaultwert: 50", dest='v2', action="store", type=int, default = 50)
parser.add_argument("--a1", help="Verzögerung in m/s² für Fahrzeug 1, Defaultwert: 6,9444¹", dest='a1', action="store", type=float, default = 25/3.6)
parser.add_argument("--a2", help="Verzögerung in m/s² für Fahrzeug 2, Defaultwert: 6,9444¹", dest='a2', action="store", type=float, default = 25/3.6)
parser.add_argument("--typ1", help="Typ des ersten Fahrzeugs. Erlaubte Werte: 0 (Defaultwert, entspricht dem Fahrzeug von Verkehrsschildern, skaliert auf 450 cm Länge und proportional 173 cm Höhe), 1 = Smart, 2 = Zoë, 3 = Landrover Defender 110, 4 = Q7, 5 = LKW⁴, 6 = Fahrrad." , dest='typ1', action="store", type=int, default = 0)
parser.add_argument("--typ2", help="Typ des zweiten Fahrzeugs. Erlaubte Werte: 0 (Defaultwert, entspricht dem Fahrzeug von Verkehrsschildern, skaliert auf 450 cm Länge und proportional 173 cm Höhe), 1 = Smart, 2 = Zoë, 3 = Landrover Defender 110, 4 = Q7, 5 = LKW⁴, 6 = Fahrrad." , dest='typ2', action="store", type=int, default = 0)
parser.add_argument("--l1", help="Länge des ersten Fahrzeugs – nur angeben, wenn man bewusst verzerren will. Default: passend zum gewählten Typ", dest='l1', action="store", type=float, default = 0)
parser.add_argument("--l2", help="Länge des zweiten Fahrzeugs – nur angeben, wenn man bewusst verzerren will. Default: passend zum gewählten Typ", dest='l2', action="store", type=float, default = 0)
parser.add_argument("--h1", help="Höhe des ersten Fahrzeugs – nur angeben, wenn man bewusst verzerren will. Default: passend zum gewählten Typ", dest='h1', action="store", type=float, default = 0)
parser.add_argument("--h2", help="Höhe des zweiten Fahrzeugs – nur angeben, wenn man bewusst verzerren will. Default: passend zum gewählten Typ", dest='h2', action="store", type=float, default = 0)
parser.add_argument("--obs", help="Typ des Hindernisses. Erlaubte Werte: 0 (Mauer), 1 (Defaultwert: laufendes Kind vom Verkehrszeichen \"Verkehrsberuhigter Bereich\"), 2 (Kleinkind mit Laufrad)⁵", dest='obs', action="store", type=int, default = 1)
parser.add_argument("--info", help="Infotext", dest='info', action="store_true")
parser.add_argument("--nourl", help="unterdrückt die Anzeige der Quelle", dest='nourl', action="store_true")

args = parser.parse_args()

if args.info:
    parser.print_help()
    print(infotext)
    sys.exit(0)

width = args.width
height = args.height
r1 = args.r1
r2 = args.r2
v1 = args.v1
v2 = args.v2
a1 = args.a1
a2 = args.a2
typ1 = args.typ1
typ2 = args.typ2
l1 = args.l1
l2 = args.l2
h1 = args.h1
h2 = args.h2
obs = args.obs

if typ1 == 1:
  car1pic = "Smart.png"
  if l1 == 0:
    l1 = 269.5
  if h1 == 0:
    h1 = 155.5
elif typ1 == 2:
  car1pic = "Zoe.png"
  if l1 == 0:
    l1 = 408.4
  if h1 == 0:
    h1 = 156.2
elif typ1 == 3:
  car1pic = "Defender.png"
  if l1 == 0:
    l1 = 478.5
  if h1 == 0:
    h1 = 203.5
elif typ1 == 4:
  car1pic = "Q7.png"
  if l1 == 0:
    l1 = 506.3
  if h1 == 0:
    h1 = 174.1
elif typ1 == 5:
  car1pic = "LKW.png"
  if l1 == 0:
    l1 = 716.0
  if h1 == 0:
    h1 = 366.0
elif typ1 == 6:
  car1pic = "Fahrrad.png"
  if l1 == 0:
    l1 = 180.0
  if h1 == 0:
    h1 = 110.0
else:
  car1pic = "PKW.png"
  if l1 == 0:
    l1 = 450.0
  if h1 == 0:
    h1 = 173.0

if typ2 == 1:
  car2pic = "Smart.png"
  if l2 == 0:
    l2 = 269.5
  if h2 == 0:
    h2 = 155.5
elif typ2 == 2:
  car2pic = "Zoe.png"
  if l2 == 0:
    l2 = 408.4
  if h2 == 0:
    h2 = 156.2
elif typ2 == 3:
  car2pic = "Defender.png"
  if l2 == 0:
    l2 = 478.5
  if h2 == 0:
    h2 = 203.5
elif typ2 == 4:
  car2pic = "Q7.png"
  if l2 == 0:
    l2 = 506.3
  if h2 == 0:
    h2 = 174.1
elif typ2 == 5:
  car2pic = "LKW.png"
  if l2 == 0:
    l2 = 716.0
  if h2 == 0:
    h2 = 366.0
elif typ2 == 6:
  car2pic = "Fahrrad.png"
  if l2 == 0:
    l2 = 180.0
  if h2 == 0:
    h2 = 110.0
else:
  car2pic = "PKW.png"
  if l2 == 0:
    l2 = 450.0
  if h2 == 0:
    h2 = 173.0

if obs == 1:
  obspic = "Kind.png"
  obsWidth = 113
  obsHeight = 150
elif obs == 2:
  obspic = "Laufrad.png"
  obsWidth = 85
  obsHeight = 90
else:
  obspic = "brickwall.png"
  obsWidth = 100
  obsHeight = 208

##Berechnetes
v01 = v1/3.6
v02 = v2/3.6
sr1 = v01*r1
sr2 = v02*r2
sb1 = 0.5 * v01 * v01 / a1
sb2 = 0.5 * v02 * v02 / a2
sa1 = sr1 + sb1
sa2 = sr2 + sb2

## Zeiten und Wege
tr1 = r1
tr2 = r2
tb1 = v01 / a1
tb2 = v02 / a2
ta1 = tr1 + tb1
ta2 = tr2 + tb2
print("Reaktionszeiten: {}s, {}s Bremszeiten: {}s, {}s, Anhaltezeiten: {}s, {}s".format(tr1,tr2,tb1,tb2,ta1,ta2))
t=0
while t <= ta2+.01:
  if t < tr1:
    s1=v01*t
  elif t <= ta1+.01:
    s1=v01*tr1+a1*tb1*(t-tr1)-.5*a1*(t-tr1)*(t-tr1)
  else:
    s1=sa1
  if t < tr2:
    s2=v02*t
  else:
    s2=v02*tr2+a2*tb2*(t-tr2)-.5*a2*(t-tr2)*(t-tr2)
  print("{:.2f} s: {:.2f} m, {:.2f} m".format(t,s1,s2))
  t=t+.2

if sa2 < sa1:
  r1, r2 = r2, r1
  v1, v2 = v2, v1
  v01, v02 = v02, v01
  sr1, sr2 = sr2, sr1
  sb1, sb2 = sb2, sb1
  sa1, sa2 = sa2, sa1
  a1, a2 = a2, a1
  l1, l2 = l2, l1
  h1, h2 = h2, h1
  car1pic, car2pic = car2pic, car1pic

headline = int(height/6.0 + 0.5)
fsheadline = int(height/18 + 0.5)
top = int(2*height/6.0 + 0.5)
bottom = int(4*height/6.0 + 0.5)
rSchild = height/18.0
rRot = rSchild*242/251
rWeiss = rSchild*175/251

rand = max(rand, rSchild+2)

if sa1 < sr2:
  v2rest = v2
else:
  v2rest = int(math.sqrt(v02 * v02 - 2 * a2 * (sa1 - sr2)) * 3.6 + 0.5)

scale = (width - 2.0 * rand) / sa2

filename="Anhalteweg_{}_{}_{:.2f}_{:.2f}_{}_vs_{}_{}_{:.2f}_{:.2f}.png".format(v1,typ1,r1,a1,obs,v2,typ2,r2,a2)
alttext ="""Veranschaulichung des Anhaltewegs zweier Fahrzeuge.
Das erste, oben dargestellte Fahrzeug fährt mit {} km/h. Nach {:.2f} Sekunden ({:.2f} m Reaktionsweg) beginnt es zu bremsen,
nach {:.2f} m Bremsweg (insgesamt {:.2f} m Anhalteweg) kommt es genau vor einem Hindernis zu stehen.
Das zweite, unten dargestellte Fahrzeug fährt mit {} km/h. Nach {:.2f} Sekunden ({:.2f} m Reaktionsweg) beginnt es zu bremsen,
nach {:.2f} m Bremsweg (insgesamt {:.2f} m Anhalteweg) kommt es zum Stehen.
Als es nach {:.2f} m das Hindernis erreichte, war es noch {} km/h schnell.
""".format(v1,r1,sr1,sb1,sa1,v2,r2,sr2,sb2,sa2,sa1,v2rest)

cs = "convert -size " + str(width) + "x" + str(height)
cs += " canvas:darkgray "
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-font {} -pointsize {} ".format(stdFont, fsheadline)
#cs += "-annotate +{}+{} \"{}\" ".format(int(width/2.0),int(headline-fsheadline),titel)

# x-Achsen-Beschriftung
cs += "-font {} -pointsize {} ".format(stdFont, 16)
for x in range(0,int(sa2)+1,10):
  cs += "-stroke \#777777 -fill none -strokewidth 1 "
  cs += "-draw \"stroke-dasharray 3 5 path 'M {},{} M {},{}'\" ".format(rand+int(x*scale), bottom + 50,rand+int(x*scale), headline)
  cs += "-stroke none -fill \#777777 -strokewidth 0 "
  cs += "-annotate +{}+{} \"{}\" ".format(rand+int(x*scale-3-(int(math.log(x+1)/math.log(10)))*5), bottom+70, x)

# Reaktions-, Brems- und Anhaltweg-Rechtecke inkl. Beschriftung und Fahrzeug

# oben
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sReaCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand,top,int(rand+scale*sr1+0.5),top+20)
cs += "-stroke black -strokewidth 1 \( -size 1x{} gradient:yellow-red -rotate -90 -write mpr:shading +delete \) ".format(int(scale*sb1+3.5))
cs += "-tile mpr:shading -draw \"rectangle {},{},{},{}\" +tile ".format(rand+int(scale*sr1+0.5),top,rand+int(scale*sr1+0.5)+int(scale*sb1+1.5),top+20)
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sAnhCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand,top+20,rand+int(scale*sa1+0.5),top+40)
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(roadCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand+int(scale*sa1+0.5),top,width-rand,top+40)
cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(rand+scale*sa1-(scale*(l1/100.0)), top-(scale*(h1/100.0)), scale*(l1/100.0), scale*(h1/100.0), car1pic)
cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(rand+scale*sa1, top-(scale*(obsHeight/100.0)), scale*(obsWidth/100.0), scale*(obsHeight/100.0), obspic)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-font {} -pointsize {} ".format(stdFont, 16)
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sr1/2), b=top+16, c=sr1)
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sr1+scale*sb1/2), b=top+16, c=sb1)
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sa1/2), b=top+36, c=sa1)
if r1 != 1 or r2 !=1:
  cs += "-annotate +{a:d}+{b:d} \"Reaktionszeit:  {c:4.2f} s\" ".format(a=int(2*rSchild+50), b=top-72, c=r1)
if a1 != 25/3.6 or a2 != 25/3.6:
  cs += "-annotate +{a:d}+{b:d} \"Verzögerung:  {c:5.2f} m/s²\" ".format(a=int(2*rSchild+50), b=top-56, c=a1)

# Linie vom Ende des Anhaltewegs nach unten
cs += "-stroke black -fill none -strokewidth 1 "
cs += "-draw \"stroke-dasharray 3 5 path 'M {},{} M {},{}'\" ".format(rand+int(scale*sa1+0.5), top, rand+int(scale*sa1+0.5), bottom+40)

# unten
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sReaCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand,bottom,int(rand+scale*sr2+0.5),bottom+20)
cs += "-stroke black -strokewidth 1 \( -size 1x{} gradient:yellow-red -rotate -90 -write mpr:shading +delete \) ".format(int(scale*sb2+2.5))
cs += "-tile mpr:shading -draw \"rectangle {},{},{},{}\" +tile ".format(rand+int(scale*sr2+0.5),bottom,rand+int(scale*sr2+0.5)+int(scale*sb2+0.5),bottom+20)
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sAnhCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand,bottom+20,rand+int(scale*sa2+0.5),bottom+40)
cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(rand+scale*sa2-(scale*(l2/100.0)), bottom-(scale*(h2/100.0)), scale*(l2/100.0), scale*(h2/100.0), car2pic)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-pointsize 16 "
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sr2/2), b=bottom+16, c=sr2)
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sr2+scale*sb2/2), b=bottom+16, c=sb2)
cs += "-annotate +{a:d}+{b:d} \"{c:5.2f} m\" ".format(a=int(rand-30+scale*sa2/2), b=bottom+36, c=sa2)
if r1 != 1 or r2 != 1:
  cs += "-annotate +{a:d}+{b:d} \"Reaktionszeit:  {c:4.2f} s\" ".format(a=int(2*rSchild+50), b=bottom-72, c=r2)
if a1 != 25/3.6 or a2 != 25/3.6:
  cs += "-annotate +{a:d}+{b:d} \"Verzögerung:  {c:5.2f} m/s²\" ".format(a=int(2*rSchild+50), b=bottom-56, c=a2)

# Geschwindigkeits-Schilder
# ToDo: ein- und dreistellige Geschwindigkeiten
cs += "-stroke black -fill \#555555 -strokewidth 1 "
cs += "-draw \"roundrectangle {},{},{},{},{},{}\" ".format(rand-5,top-int(3*rSchild)-5,rand+5, top, 2, 2)
cs += "-draw \"roundrectangle {},{},{},{},{},{}\" ".format(rand-5,top-int(3*rSchild)-7,rand+5, top-int(3*rSchild)-2, 5, 5)
cs += "-stroke black -fill white -strokewidth 1 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,top-int(2*rSchild),rand,top-int(1*rSchild))
cs += "-stroke red -fill red -strokewidth 0 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,top-int(2*rSchild),rand,top-int(2*rSchild+rRot))
cs += "-stroke white -fill white -strokewidth 0 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,top-int(2*rSchild),rand,top-int(2*rSchild+rWeiss))
cs += "-fill black -strokewidth 0 -stroke none "
if v1 > 99:
    cs += "-font {} -pointsize {} ".format(signFont, int(fsheadline*0.9))
    cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/1.7),top-int(1.7*rSchild),v1)
else:
    cs += "-font {} -pointsize {} ".format(signFont, int(fsheadline*1.1))
    if v1 < 10:
        cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/4.0),top-int(1.6*rSchild),v1)
    else:
        cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/2.0),top-int(1.6*rSchild),v1)    
cs += "-stroke black -fill \#555555 -strokewidth 1 "
cs += "-draw \"roundrectangle {},{},{},{},{},{}\" ".format(rand-5,bottom-int(3*rSchild)-5,rand+5, bottom, 2, 2)
cs += "-draw \"roundrectangle {},{},{},{},{},{}\" ".format(rand-5,bottom-int(3*rSchild)-7,rand+5, bottom-int(3*rSchild)-2, 5, 5)
cs += "-stroke black -fill white -strokewidth 1 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,bottom-int(2*rSchild),rand,bottom-int(1*rSchild))
cs += "-stroke red -fill red -strokewidth 0 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,bottom-int(2*rSchild),rand,bottom-int(2*rSchild+rRot))
cs += "-stroke white -fill white -strokewidth 0 "
cs += "-draw \"circle {},{},{},{}\" ".format(rand,bottom-int(2*rSchild),rand,bottom-int(2*rSchild+rWeiss))
cs += "-fill black -strokewidth 0 -stroke none "
if v2 > 99:
    cs += "-font {} -pointsize {} ".format(signFont, int(fsheadline*0.9))
    cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/1.7),bottom-int(1.7*rSchild),v2)
else:
    cs += "-font {} -pointsize {} ".format(signFont, int(fsheadline*1.1))
    if v2 < 10:
        cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/4.0),bottom-int(1.6*rSchild),v2)
    else:
        cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/2.0),bottom-int(1.6*rSchild),v2)    
#cs += "-pointsize {} ".format(int(fsheadline*1.1))
#cs += "-annotate +{}+{} \"{}\" ".format(rand-int(fsheadline/2.0),bottom-int(1.6*rSchild),v2)

# Restgeschwindigkeit
cs += "-fill \#{} -strokewidth 0 -stroke none ".format(vRestCol)
cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(rand+scale*sa1-int(1.3*fsheadline), bottom-12-int(fsheadline*1.9), int(2.5*fsheadline), int(2.5*fsheadline), "boom.png")
cs += "-pointsize {} ".format(int(fsheadline*1))
if v2rest < 10:
    cs += "-annotate +{}+{} \"{}\" ".format(int(rand+scale*sa1-int(fsheadline/4.0)),bottom-10-int(fsheadline/2.0),v2rest)
else:
    cs += "-annotate +{}+{} \"{}\" ".format(int(rand+scale*sa1-int(fsheadline/2.0)),bottom-10-int(fsheadline/2.0),v2rest)

#Legende
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sReaCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand,bottom+100,rand+20,bottom+120)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-font {} -pointsize {} ".format(stdFont, 16)
cs += "-annotate +{}+{} \"{}\" ".format(rand+30,bottom+116,"Reaktionsweg")
#cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sBrCol)
#cs += "-draw \"rectangle {},{},{},{}\" ".format(rand+200,bottom+100,rand+220,bottom+120)
cs += "-stroke black -strokewidth 1 \( -size 1x22 gradient:yellow-red -rotate -90 -write mpr:shading +delete \) "
cs += "-tile mpr:shading -draw \"rectangle {},{},{},{}\" +tile ".format(rand+200,bottom+100,rand+220,bottom+120)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-pointsize 16 "
cs += "-annotate +{}+{} \"{}\" ".format(rand+230,bottom+116,"Bremsweg")
cs += "-stroke black -fill \#{} -strokewidth 1 ".format(sAnhCol)
cs += "-draw \"rectangle {},{},{},{}\" ".format(rand, bottom+140,rand+20,bottom+160)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-pointsize 16 "
cs += "-annotate +{}+{} \"{}\" ".format(rand+30,bottom+156,"Anhalteweg")
cs += "-fill \#{} -strokewidth 0 -stroke none ".format(vRestCol)
cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(rand+188, bottom+130, 40 , 43, "boom.png")
cs += "-font {} -pointsize 18 ".format(signFont)
if v2rest < 10:
    cs += "-annotate +{}+{} \"{}\" ".format(rand+205,bottom+156,v2rest)
else:
    cs += "-annotate +{}+{} \"{}\" ".format(rand+200,bottom+156,v2rest)
cs += "-fill black -strokewidth 0 -stroke none "
cs += "-font {} -pointsize 16 ".format(stdFont)
cs += "-annotate +{}+{} \"{}\" ".format(rand+230,bottom+156,"Restgeschwindigkeit beim Aufprall in km/h")

if not args.nourl:
  cs += "-pointsize 12 "
  cs += "-annotate +{}+{} \"{}\" ".format(width-192,height-10,"https://gitlab.com/joschtl/bremsweg")
  #cs += "-font {} ".format(signFont)
  #cs += "-annotate +{}+{} \"{}\" ".format(width-120,height-20,"POI17F1.de")

cs += filename

os.system(cs)
print(alttext)
